# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    from game import Directions
    
    visited = []
    firstMove = True
    stack = util.Stack()
    stack.push(problem.getStartState())
    while not stack.isEmpty():
        if firstMove:
            start = dict()
            startPlace = stack.pop()
            start['parent'] = None
            start['direction'] = None
            start['coord'] = startPlace
            firstMove = False
        else:
            start = stack.pop()
            
        startMove = start['coord']
        
        if problem.isGoalState(startMove):
            print("found goal state!")
            break
            
        if startMove not in visited:
            visited.append(startMove)
            for succ in problem.getSuccessors(startMove):
                if succ[0] not in visited:
                    temp = dict()
                    temp['parent'] = start
                    temp['direction'] = succ[1]
                    temp['coord'] = succ[0]
                    stack.push(temp)
                    
    returnList = []
    while start['parent'] != None:
        returnList.append(start['direction'])
        start = start['parent']
    return returnList[::-1]
    
    
def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    from game import Directions
    
    visited = []
    firstMove = True
    queue = util.Queue()
    queue.push(problem.getStartState())
    while not queue.isEmpty():
        if firstMove:
            start = dict()
            startPlace = queue.pop()
            start['parent'] = None
            start['direction'] = None
            start['coord'] = startPlace
            firstMove = False
        else:
            start = queue.pop()
            
        startMove = start['coord']
        
        if problem.isGoalState(start['coord']):
            break
            
        if startMove not in visited:
            visited.append(startMove)
            for succ in problem.getSuccessors(start['coord']):
                if succ[0] not in visited:
                    temp = dict()
                    temp['parent'] = start
                    temp['direction'] = succ[1]
                    temp['coord'] = succ[0]
                    queue.push(temp)
                    
    returnList = []
    while start['parent'] != None:
        returnList.append(start['direction'])
        start = start['parent']
    return returnList[::-1]

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    import util
    from game import Directions
    queue = util.PriorityQueue()
    visited = []
    firstMove = True
    queue.push(problem.getStartState(),0)
    while not queue.isEmpty():
        if firstMove:
            start = dict()
            startPlace = queue.pop()
            start['parent'] = None
            start['direction'] = None
            start['cost'] = 0
            start['coord'] = startPlace
            firstMove = False
        else:
            start = queue.pop()
                    
        if problem.isGoalState(start['coord']):
            break
            
        if start['coord'] not in visited:
            visited.append(start['coord'])
            for succ in problem.getSuccessors(start['coord']):
                if succ[0] not in visited:
                    temp = dict()
                    temp['parent'] = start
                    temp['coord'] = succ[0]
                    temp['direction'] = succ[1]
                    temp['cost'] = succ[2] + start['cost']
                    queue.push(temp,temp['cost'])
                    
    returnList = []
    while start['parent'] != None:
        returnList.append(start['direction'])
        start = start['parent']
    return returnList[::-1]
        
        
def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    import util
    queue = util.PriorityQueue()
    visited = []
    firstMove = True
    queue.push(problem.getStartState(),0 + heuristic(problem.getStartState(),problem))
    while not queue.isEmpty():
        if firstMove:
            start = dict()
            startPlace = queue.pop()
            start['parent'] = None
            start['direction'] = None
            start['cost'] = 0
            start['coord'] = startPlace
            firstMove = False
        else:
            start = queue.pop()
            
        if problem.isGoalState(start['coord']):
            print("found goal state!")
            break
        #print(queue.heap)
        #print()
        if start['coord'] not in visited:
            visited.append(start['coord'])
            for succ in problem.getSuccessors(start['coord']):
                if succ[0] not in visited:
                    cost = start['cost'] + succ[2]
                    temp = dict()
                    temp['parent'] = start
                    temp['coord'] = succ[0]
                    temp['direction'] = succ[1]
                    temp['cost'] = cost
                    queue.push(temp,cost + heuristic(succ[0],problem))
                    
    returnList = []
    while start['parent'] != None:
        returnList.append(start['direction'])
        start = start['parent']
    return returnList[::-1]


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
